const server = [
  {
    id: 1,
    uker: "BRI UNIT_MDN_SIBOLGA_PANDAN",
    zone: 1,
    longitude: 98.8260017,
    latitude: 1.6777917
  },
  {
    id: 2,
    uker: "BRI ATM JAKARTA 2 SPBU 34-41352 KARAWANG",
    zone: 2,
    longitude: 107.0538083,
    latitude: -6.2646017
  },
  {
    id: 3,
    uker: "BRI KANCA BACKUP_ YGY_SOLO BARU",
    zone: 3,
    longitude: 110.8185831,
    latitude: -7.6003854
  },
  {
    id: 4,
    uker: "BRI ATM_DPS_RUTENG_ATM CENTER DENNY S MART",
    zone: 4,
    longitude: 119.894548,
    latitude: -8.490725
  },
  {
    id: 5,
    uker: "BRI ATM_BJM_SAMARINDA_HOTEL HORIZON",
    zone: 5,
    longitude: 117.1525437,
    latitude: -0.4991701
  }
];

export { server };
