// For more information on data-driven styles, see https://www.mapbox.com/help/gl-dds-ref/
export const dataLayer = {
  id: "data",
  type: "fill",
  paint: {
    "fill-color": {
      property: "ID",
      stops: [
        [1, "#3288bd"],
        [2, "#66c2a5"],
        [3, "#abdda4"],
        [4, "#e6f598"],
        [5, "#ffffbf"]
      ]
    },
    "fill-opacity": 0.5
  }
};
