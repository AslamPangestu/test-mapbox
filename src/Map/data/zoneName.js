const zone = [
  {
    id: 1,
    name: "Zona 1",
    longitude: 102.1486693,
    latitude: -1.0184006
  },
  {
    id: 2,
    name: "Zona 2",
    longitude: 110.9518233,
    latitude: -0.1393006
  },
  {
    id: 3,
    name: "Zona 3",
    longitude: 108.9864045,
    latitude: -7.2906968
  },
  {
    id: 4,
    name: "Zona 4",
    longitude: 118.72667,
    latitude: -8.46006
  },
  {
    id: 5,
    name: "Zona 5",
    longitude: 120.2502303,
    latitude: -2.2892493
  }
];
export { zone };
