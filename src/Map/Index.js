import React from "react";
import MapGL, { Source, Layer, Marker, Popup } from "react-map-gl";
import { dataLayer } from "./mapStyle";
import { updatePercentiles } from "./utils";
import { json as requestJson } from "d3-request";
import { FaCircle } from "react-icons/fa";
import { server as ServerData } from "./data/serverLoc";
import { zone as ZoneData } from "./data/zoneName";

import dataset from "./data/dataset.json";
import { DataServer } from "./data/dataserver";

class Map extends React.PureComponent {
  state = {
    data: dataset,
    showPopup: false,
    hoveredFeature: null,
    selectedZone: null,
    serverData: DataServer,
    popupData: {
      id: 0,
      kanwil: "",
      offline_date: "",
      latitude: 0,
      longitude: 0
    },
    viewport: {
      width: 400,
      height: 400,
      latitude: -0.7893,
      longitude: 113.9213,
      zoom: 4
    }
  };

  _showData = data => {
    if (this.state.popupData.id === data.id) {
      this.setState({
        showPopup: false,
        popupData: {
          id: 0,
          kanwil: "",
          offline_date: "",
          latitude: 0,
          longitude: 0
        }
      });
    } else {
      this.setState({
        showPopup: true,
        popupData: {
          id: data.id,
          kanwil: data.kanwil,
          offline_date: data.offline_date,
          latitude: data.latitude,
          longitude: data.longitude
        }
      });
    }
  };

  _loadData = (data, id) => {
    // updatePercentiles(data, f => f.properties.income[this.state.year]);
    if (id === 1) this.setState({ data });
    else if (id === 2) this.setState({ serverDate: data });
  };

  _onViewportChange = viewport => this.setState({ viewport });
  _onClick = event => {
    const {
      features,
      srcEvent: { offsetX, offsetY }
    } = event;
    const hoveredFeature =
      features && features.find(f => f.layer.id === "data");
    if (hoveredFeature !== undefined) {
      let zonePos = ZoneData.find(
        item => item.id === hoveredFeature.properties.ID
      );
      this.setState({
        viewport: {
          ...this.state.viewport,
          latitude: zonePos.latitude,
          longitude: zonePos.longitude,
          // latitude: -2.2892493,
          // longitude: 120.2502303,
          zoom: 5
        },
        selectedZone: hoveredFeature.properties.ID
      });
      this.props.setZone(hoveredFeature.properties.ID);
    }
    this.setState({
      hoveredFeature,
      x: offsetX,
      y: offsetY
    });
  };

  _renderTooltip() {
    const { hoveredFeature, x, y } = this.state;

    return (
      hoveredFeature && (
        <div className="tooltip" style={{ left: x, top: y }}>
          <div>State: {hoveredFeature.properties.name}</div>
        </div>
      )
    );
  }

  render() {
    const {
      serverData,
      selectedZone,
      data,
      showPopup,
      viewport,
      popupData
    } = this.state;

    const { dataServer, zone } = this.props;
    console.log("popup", showPopup);
    return (
      <div style={{ height: "100%" }}>
        <MapGL
          {...viewport}
          width="100vw"
          height="100vh"
          mapStyle="mapbox://styles/mapbox/light-v9"
          onViewportChange={this._onViewportChange}
          onClick={this._onClick}
          // onHover={this._onHover}
          mapboxApiAccessToken={
            "pk.eyJ1IjoiYXNsYW1wYW5nZXN0dSIsImEiOiJjazIzM3NmYTMwaHk5M25xb3hxM3IwYmFpIn0.3-Kpn8x0CpcHXy0dgjaGdA"
          }
        >
          {dataServer.map(item => (
            <>
              {zone === item.zone &&
                zone !== null &&
                item.offline_date !== "" &&
                !Number.isInteger(item.latitude) &&
                !Number.isInteger(item.longitude) && (
                  <Marker latitude={item.latitude} longitude={item.longitude}>
                    <FaCircle
                      onClick={() => this._showData(item)}
                      color={"red"}
                      size={8}
                    />
                  </Marker>
                )}
            </>
          ))}
          {ZoneData.map(item => (
            <Marker latitude={item.latitude} longitude={item.longitude}>
              <div>{item.name}</div>
            </Marker>
          ))}
          {showPopup && (
            <Popup
              latitude={popupData.latitude}
              longitude={popupData.longitude}
              closeButton={true}
              closeOnClick={false}
              onClose={() => this.setState({ showPopup: false })}
              anchor="bottom"
            >
              <b>Detail</b>
              <div>Kanwil : {popupData.kanwil}</div>
              <div>Cabang</div>
              <div>Ga Tau</div>
              <div>Waktu Mati : {popupData.offline_date}</div>
              <div>Keterangan : </div>
            </Popup>
          )}
          <Source type="geojson" data={data}>
            <Layer {...dataLayer} />
          </Source>
          {this._renderTooltip()}
        </MapGL>
      </div>
    );
  }
}

export default Map;
