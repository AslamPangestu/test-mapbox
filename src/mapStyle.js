// For more information on data-driven styles, see https://www.mapbox.com/help/gl-dds-ref/
export const dataLayer = {
  id: "data",
  type: "fill",
  paint: {
    "fill-color": {
      property: "ID",
      stops: [
        [1, "#9b59b6"],
        [2, "#0984e3"],
        [3, "#d35400"],
        [4, "#27ae60"],
        [5, "#f6e58d"]
      ]
    },
    "fill-opacity": 0.25
  }
};
